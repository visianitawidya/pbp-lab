from django.urls import path
from .views import index,note_list,add_note

urlpatterns = [
    path('', index, name='index'),
    path('note-list', note_list, name='notes_list'),
    path('add-note', add_note, name='add_note')
]