from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from .models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    response = {'notes' : Note.objects.all()}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    response = {'notes' : Note.objects.all()}
    return render(request, 'lab4_note_list.html', response)

def add_note(request):
    context ={}

    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)

    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        if request.method == 'POST':
            return HttpResponseRedirect('/lab-4')
    
    context['form']= form
    return render(request, "lab4_form.html", context)