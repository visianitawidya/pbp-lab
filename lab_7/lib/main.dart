import 'package:flutter/material.dart';
import 'package:lab_7/model/user.dart';
import 'package:lab_7/util/user_preferences.dart';
import 'package:lab_7/widgets/button_widget.dart';
import 'package:lab_7/widgets/textfield_widget.dart';
import 'package:lab_7/widgets/profilepicture_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 6 - PBP',
      theme: ThemeData(
        canvasColor: const Color.fromRGBO(255, 254, 229, 1),
      ),
      home: const EditProfilePage(title: 'Edit Profile'),
    );
  }
}

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  User user = UserPreferences.myUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        title: Text(widget.title),
        backgroundColor: const Color.fromRGBO(59, 148, 94, 1.0),
      ),
      body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 32),
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 16, 0, 40),
              child: ProfileWidget(
                imagePath: user.imagePath,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 4, 0, 8),
              child: TextFieldWidget(
                label: 'First Name',
                text: user.firstName,
                onChanged: (firstName) {},
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 4, 0, 8),
              child: TextFieldWidget(
                label: 'Last Name',
                text: user.lastName,
                onChanged: (lastName) {},
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 4, 0, 8),
              child: TextFieldWidget(
                label: 'Bio',
                text: user.bio,
                maxLines: 5,
                onChanged: (bio) {},
              ),
            ),
            // ignore: prefer_const_constructors
            Padding(
                padding: const EdgeInsets.all(16.0),
                child: ButtonWidget(
                  text: 'Save Changes',
                ))
          ]),
    );
  }
}
