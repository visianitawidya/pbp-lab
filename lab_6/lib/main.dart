import 'package:flutter/material.dart';
import 'package:lab_6/model/user.dart';
import 'package:lab_6/util/user_preferences.dart';
import 'package:lab_6/widgets/button_widget.dart';
import 'package:lab_6/widgets/textfield_widget.dart';
import 'package:lab_6/widgets/profilepicture_widget.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 6 - PBP',
      theme: ThemeData(
        canvasColor: const Color.fromRGBO(255, 254, 229, 1),
      ),
      home: const EditProfilePage(title: 'Edit Profile'),
    );
  }
}

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  User user = UserPreferences.myUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(),
        title: Text(widget.title),
        backgroundColor: const Color.fromRGBO(59, 148, 94, 1.0),
      ),
      body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 32),
          physics: BouncingScrollPhysics(),
          children: [
            ProfileWidget(
              imagePath: user.imagePath,
            ),
            TextFieldWidget(
              label: 'First Name',
              text: user.firstName,
              onChanged: (firstName) {},
            ),
            TextFieldWidget(
              label: 'Last Name',
              text: user.lastName,
              onChanged: (lastName) {},
            ),
            TextFieldWidget(
              label: 'Bio',
              text: user.bio,
              maxLines: 5,
              onChanged: (bio) {},
            ),
            ButtonWidget(
              text: 'Save Changes',
              onClicked: () {
                setState(() {});
              },
            )
          ]),
    );
  }
}
