class User {
  final String imagePath;
  final String firstName;
  final String lastName;
  final String bio;

  const User({
    required this.imagePath,
    required this.firstName,
    required this.lastName,
    required this.bio,
  });
}
