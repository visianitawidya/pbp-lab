1. Apakah perbedaan antara JSON dan XML?
    XML dan JSON merupakan format untuk pertukaran data. Namun, keduanya memiliki perbedaan. Perbedaan mendasar dari keduanya adalah, JSON hanyalah format data sedangkan XML adalah bahasa markup. Berikut adalah perbedaan lain di antara keduanya:
    a. Data XML disimpan sebagai tree structure, sedangkan data JSON disimpan seperti map dengan pasangan key value.
    b. XML tidak mendukung array secara langsung. Untuk dapat, tetapi dapat menggunakan tambahan tag untuk setiap item. Di sisi lain, JSON mendukung array dan kontenny dapat diakses.
    c. XML mendukung banyak tipe data kompleks, sedangkan JSON hanya mendukung tipe data primitif.
    d. XML buka bahasa pemrograman, tetapi memiliki tag untuk mendefinsikan elemen. Sedangkan, JSON merupakan format yang ditulis dengan bahasa JavaScript.
    e. XML memiliki ukuran dokumen besar dan cenderung lebih sulit dibaca, sedangkan JSON lebih mudah dibaca karena penulisannya ringkas

2. Apakah perbedaan antara HTML dan XML?
    XML dan HTML adalah bahasa markup yang digunakan untuk membuat halaman web dan aplikasi web. HTML (HyperText Markup Language) dibuat untuk merancang bagaimana data ditampilkan dan memfasilitasi dalam tranfer dokumen, sedangkan XML (eXtensible Markup Language) digunakan untuk mengangkut data dan fokus pada kontennya.
    Berikut adalah perbedaan lain di antara keduanya:
    a. XML berfokus pada transfer data, sedangkan HTML difokuskan pada penyajian data.
    b. XML bersifat Case Sensitive, sedangkan HTML bersifat Case Insensitive.
    c. XML menyediakan dukungan namespaces, sedangkan HTML tidak menyediakan dukungan namespaces.
    d. XML ketat untuk tag penutup, sedangkan HTML cenderung tidak ketat untuk tag penutup.
    e. Tag XML dapat dikembangkan dan  tidak ditentukan sebelumnya, sedangkan HTML memiliki tag terbatas pada tag yang telah ditentukan sebelumnya.

Refrensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html
