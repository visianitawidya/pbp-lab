from django.forms import widgets
from lab_1.models import Friend
from django import forms

#widget datepicker
class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        widgets = {
            'dob' : DateInput()
        }


